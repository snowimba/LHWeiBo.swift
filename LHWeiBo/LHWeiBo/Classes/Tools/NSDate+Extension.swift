//
//  NSDate+Extension.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/25.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import Foundation

extension NSDate {

	class func getDateWithString(dateStr: String) -> NSDate {

		commonDF.dateFormat = "EEE MMM dd HH:mm:ss z yyyy"
		commonDF.locale = NSLocale(localeIdentifier: "en_US")
		let createdDate = commonDF.dateFromString(dateStr)!

		return createdDate
	}

	func getStringWithDate() -> String {

		let calendar = NSCalendar.currentCalendar()

		if isNowYear(self) {

			if calendar.isDateInToday(self) {

				let num = self.timeIntervalSinceNow

				let num32 = abs(Int32(num))

				if num32 < 60 {
					return "刚刚"
				} else if num32 < 60 * 60 {
					return "\(num32/60)分钟前"
				} else {

					return "\(num32/60/60)小时前"
				}
			} else if calendar.isDateInYesterday(self) {

				commonDF.dateFormat = "昨天 HH:mm"
				return commonDF.stringFromDate(self)
			} else {

				commonDF.dateFormat = "MM-dd HH:mm"
				return commonDF.stringFromDate(self)
			}
		} else {

			commonDF.dateFormat = "yyyy-MM-dd HH:mm"
			return commonDF.stringFromDate(self)
		}
	}

	private func isNowYear(date: NSDate) -> Bool {

		commonDF.dateFormat = "yyyy"
		let cteaY = commonDF.stringFromDate(date)
		let nowY = commonDF.stringFromDate(NSDate())

		return cteaY == nowY
	}
}