//
//  LHSessionManager.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/20.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit
import AFNetworking

enum LHRequestMethod: String {
	case GET = "GET"
	case POST = "POST"
}

class LHSessionManager: AFHTTPSessionManager {
	
	static let sessionManager : LHSessionManager = {
		
		let instance = LHSessionManager()
		
		instance.responseSerializer.acceptableContentTypes?.insert("text/html")
		instance.responseSerializer.acceptableContentTypes?.insert("text/plain")
		return instance
	}()
	
	typealias LHRequestBlack = (response: AnyObject?, error: NSError?) -> ()
	
	func request(requestMethod: LHRequestMethod = .GET, URLString: String, parameters: AnyObject?, finishBack: LHRequestBlack) {
		
		let success = { (dataTask: NSURLSessionDataTask, responseObject: AnyObject?) -> Void in
			finishBack(response: responseObject, error: nil)
		}
		
		let failure = { (dataTask: NSURLSessionDataTask?, error: NSError) -> Void in
			finishBack(response: nil, error: error)
		}
		
		if requestMethod == .GET {
			
			GET(URLString, parameters: parameters, progress: nil, success: success, failure: failure)
		} else {
			
			POST(URLString, parameters: parameters, progress: nil, success: success, failure: failure)
		}
	}
}
