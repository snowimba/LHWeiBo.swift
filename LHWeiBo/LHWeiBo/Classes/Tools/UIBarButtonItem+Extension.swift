//
//  UIBarButtonItem+Extension.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/18.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
	
	convenience init(title : String? = nil, imageName: String? = nil, target: AnyObject?, action: Selector) {
		self.init()
		let btn : UIButton = UIButton()
		if title != nil {
			
			
			btn.setTitle(title, forState: UIControlState.Normal)
			btn.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
			btn.setTitleColor(UIColor.orangeColor(), forState: UIControlState.Highlighted)
			btn.titleLabel?.font = UIFont.systemFontOfSize(14)
		}
		
		if imageName != nil {
			
			btn.setImage(UIImage(named: imageName!), forState: UIControlState.Normal)
			
			btn.setImage(UIImage(named: "\(imageName!)_highlighted"), forState: UIControlState.Highlighted)
		}
		
		btn.addTarget(target, action: action, forControlEvents: UIControlEvents.TouchUpInside)
		
		btn.sizeToFit()
		
		customView = btn
	}
}
