//
//  UIView+Expand.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/18.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

extension UIView {
	
	
	@IBInspectable var borderWidth : CGFloat {
		
		get {
			return layer.borderWidth
		}
		set {
			
			layer.borderWidth = newValue
			
		}
		
	}
	
	@IBInspectable var borderColor : UIColor {
		
		get {
			
			return UIColor(CGColor: layer.borderColor!)
			
		}
		set {
			
			layer.borderColor = newValue.CGColor
			
		}
		
	}
	
	@IBInspectable var cornerRadius : CGFloat {
		
		get {
			
			return layer.cornerRadius
			
		}
		set {
			
			layer.cornerRadius = newValue
			
			if layer.cornerRadius > 0 {
            
				clipsToBounds = true
				
			}
		}
	}
}
