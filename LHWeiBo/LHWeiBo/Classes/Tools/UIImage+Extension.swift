//
//  UIImage+Extension.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/25.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import Foundation

extension UIImage {

	class func getBGImage() -> UIImage {

		let window = UIApplication.sharedApplication().keyWindow
		UIGraphicsBeginImageContextWithOptions(window!.frame.size, false, 0)

		window?.drawViewHierarchyInRect(window!.frame, afterScreenUpdates: false)

		let image = UIGraphicsGetImageFromCurrentImageContext()

		UIGraphicsEndImageContext()

		return image
	}
}