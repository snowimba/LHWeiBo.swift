//
//  LHEonticViewModel.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/28.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

let LHCol = 7
let LHRow = 3
let EmoticNum = LHCol * LHRow - 1

class LHEmoticViewModel: NSObject {

	static let sharedViewModel : LHEmoticViewModel = {

		let instance = LHEmoticViewModel()

		return instance
	}()

//    var allEontics : []

//    var emotics : [LHEonticM] {
//
//
//
//    }

	lazy var recentEmotic : [LHEonticM] = {

		return [LHEonticM]()
	}()

	lazy var allEmotics : [[[LHEonticM]]] = {
		return [
			[self.recentEmotic],
				self.subArrEmotics("default/info.plist"),
				self.subArrEmotics("emoji/info.plist"),
				self.subArrEmotics("lxh/info.plist"),
		]
	}()

	func subArrEmotics(handStr: String) -> [[LHEonticM]] {

		let temArr = getEmoticData(handStr)

		let index = (temArr.count + 19) / EmoticNum

		var subArr = [[LHEonticM]]()

		for i in 0..<index {

			var range = NSMakeRange(i * EmoticNum, EmoticNum)

			let len = range.length + range.location

			if len > temArr.count {

				range.length = temArr.count - range.location
			}

			let arr = (temArr as NSArray).subarrayWithRange(range) as! [LHEonticM]

			subArr.append(arr)
		}
		return subArr
	}

	lazy var bundle : NSBundle = {

		let path = NSBundle.mainBundle().pathForResource("Emoticons.bundle", ofType: nil)

		return NSBundle(path: path!)!
	}()

	func getEmoticData(handStr: String) -> [LHEonticM] {

//"default/info.plist"

		let str = bundle.pathForResource(handStr, ofType: nil)

		let arr = NSArray(contentsOfFile: str!)

		var temArr = [LHEonticM]()

		for dict in arr! {

			let eM = LHEonticM(dict: dict as! [String : AnyObject])
			eM.name = (str! as NSString).stringByDeletingLastPathComponent
			temArr.append(eM)
		}

		return temArr
	}
}
