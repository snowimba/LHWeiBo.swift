//
//  LHStatuseCellViewModel.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/22.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHStatuseCellViewModel: NSObject {

	var status : LHStatusM?

	var vipViewImage : UIImage?

	var verifiedImage : UIImage?

	var retweet : String?

	var comment : String?

	var unlike : String?

	var retweetText : String?

	var created_at : String? {

		return status?.createdDate?.getStringWithDate()
	}

	var source : String?

//	private func createdTime() {
//
//		guard let creaT = status?.created_at else {
//
//			return
//		}
//
////         "created_at": "Tue May 31 17:46:55 +0800 2011",
//		commonDF.dateFormat = "EEE MMM dd HH:mm:ss z yyyy"
//		commonDF.locale = NSLocale(localeIdentifier: "en_US")
//		let createdDate = commonDF.dateFromString(creaT)!
//
////		print(createdDate)
//
////		createdDate = createdDate.dateByAddingTimeInterval(-60 * 60 * 10)
//
//		let calendar = NSCalendar.currentCalendar()
//
//		if isNowYear(createdDate) {
//
//			if calendar.isDateInToday(createdDate) {
//
//				let num = createdDate.timeIntervalSinceNow
//
//				let num32 = abs(Int32(num))
//
//				if num32 < 60 {
//					created_at = "刚刚"
//				} else if num32 < 60 * 60 {
//					created_at = "\(num32/60)分钟前"
//				} else {
//
//					created_at = "\(num32/60/60)小时前"
//				}
//			} else if calendar.isDateInYesterday(createdDate) {
//
//				commonDF.dateFormat = "昨天 HH:mm"
//				created_at = commonDF.stringFromDate(createdDate)
//			} else {
//
//				commonDF.dateFormat = "MM-dd HH:mm"
//				created_at = commonDF.stringFromDate(createdDate)
//			}
//		} else {
//
//			commonDF.dateFormat = "yyyy-MM-dd HH:mm"
//			created_at = commonDF.stringFromDate(createdDate)
//		}
//	}
//
//	private func isNowYear(date: NSDate) -> Bool {
//
//		commonDF.dateFormat = "yyyy"
//		let cteaY = commonDF.stringFromDate(date)
//		let nowY = commonDF.stringFromDate(NSDate())
//
//		return cteaY == nowY
//	}

	private func sourceStr() {

		if let sStr = status?.source {
//        source": "<a href="http://weibo.com" rel="nofollow">新浪微博</a>",
			if let stIndex = sStr.rangeOfString("\">") {
				if let enIndex = sStr.rangeOfString("</") {

					source = sStr.substringWithRange(stIndex.endIndex..<enIndex.startIndex)
				}
			}
		}
	}

	init(statuses: LHStatusM) {

		super.init()

		self.status = statuses

		if let name = statuses.retweeted_status?.user?.screen_name,
			let text = statuses.retweeted_status?.text {
			retweetText = "@\(name):\(text)"
		}
		setupUI()
//		changeCount()
//		createdTime()
		sourceStr()
	}

	private func changeCount(count: Int) -> String? {

		if count <= 0 {

			return nil
		} else if count < 10000 {

			return "\(count)"
		} else {

			let num : Int = count / 1000

			let f : CGFloat = CGFloat(num) / 10.0

			var str : String = "\(f)万"

			if (str.rangeOfString(".0") != nil) {

				str = str.stringByReplacingOccurrencesOfString(".0", withString: "")
			}

			return str
		}
	}

	private func setupUI() {

		retweet = changeCount(status!.reposts_count)
		comment = changeCount(status!.comments_count)
		unlike = changeCount(status!.attitudes_count)

		let rank : Int = status!.user!.mbrank

		if rank > 0 && rank < 7 {

			vipViewImage = UIImage(named: "common_icon_membership_level\(rank)")
		} else {

			vipViewImage = nil
		}

		let verified : Int = status?.user?.verified ?? -1

		switch verified {

		case 1:
			verifiedImage = UIImage(named: "avatar_vip")
		case 2, 3, 5:
			verifiedImage = UIImage(named: "avatar_enterprise_vip")
		case 220:
			verifiedImage = UIImage(named: "avatar_vgirl")
			default :
			verifiedImage = nil
		}
	}
}
