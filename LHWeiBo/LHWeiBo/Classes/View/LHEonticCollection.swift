//
//  LHEonticCollection.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/28.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHEonticCell: UICollectionViewCell {

	var emotics : [LHEonticM]? {

		didSet {

			deleBtn.hidden = !(emotics?.count > 0)

			for btn in btnArr {

				btn.hidden = true
			}

			for (index, emotic) in emotics!.enumerate() {
				let btn = btnArr[index]
				btn.hidden = false
				if emotic.type == "1" {

					btn.setTitle((emotic.code! as NSString).emoji(), forState: UIControlState.Normal)
					btn.setImage(nil, forState: UIControlState.Normal)
				} else {
					btn.setTitle(nil, forState: UIControlState.Normal)
					btn.setImage(UIImage(named: "\(emotic.name!)/\(emotic.png!)"), forState: UIControlState.Normal)
				}
			}
		}
	}

	private lazy var btnArr : [UIButton] = [UIButton]()

	override init(frame: CGRect) {
		super.init(frame: frame)

		setupUI()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func layoutSubviews() {
		super.layoutSubviews()

		let btnW = self.frame.width / CGFloat(7)
		let btnH = self.frame.height / CGFloat(3)

		deleBtn.frame = CGRectMake(btnW * 6, btnH * 2, btnW, btnH)

		for (index, btn) in btnArr.enumerate() {

			btn.frame.size = CGSizeMake(btnW, btnH)

			btn.frame.origin = CGPointMake(btnW * CGFloat(Int(index % 7)), btnH * CGFloat(Int(index / 7)))
		}
	}

	private lazy var deleBtn : UIButton = {

		let btn = UIButton()

		btn.setImage(UIImage(named: "compose_emotion_delete"), forState: UIControlState.Normal)
		btn.setImage(UIImage(named: "compose_emotion_delete_highlighted"), forState: UIControlState.Highlighted)

		return btn
	}()

	private func setupUI() {

		contentView.addSubview(deleBtn)
		for _ in 0..<20 {

			let btn = UIButton()

			btn.titleLabel?.font = UIFont.systemFontOfSize(30)

			addSubview(btn)

			btnArr.append(btn)
		}
	}
}
