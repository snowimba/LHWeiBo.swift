//
//  LHSendController.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/25.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHSendController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		setupUI()
		view.backgroundColor = UIColor.orangeColor()
		// Do any additional setup after loading the view.

		NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillChangeFrame:", name: UIKeyboardWillChangeFrameNotification, object: nil)
	}

	deinit {

		NSNotificationCenter.defaultCenter().removeObserver(self)
	}

	private var isAmin : Bool = true

	@objc func keyboardWillChangeFrame(noti: NSNotification) {

		if !isAmin {

			return
		}

		let dict = noti.userInfo
//        UIKeyboardFrameEndUserInfoKey
		let rect = (dict!["UIKeyboardFrameEndUserInfoKey"] as! NSValue).CGRectValue()

		toolBar.snp_updateConstraints { (make) -> Void in
			make.bottom.equalTo(rect.origin.y - self.view.frame.height)
		}

		UIView.animateWithDuration(0.25) { () -> Void in

			self.view.layoutIfNeeded()
		}
	}

	@objc private func emoticonBtnClick() {

		textView.inputView = (textView.inputView == nil) ? emoticonView : nil

		isAmin = false
		textView.resignFirstResponder()
		isAmin = true
		textView.becomeFirstResponder()
	}

	private lazy var emoticonView : LHEmoticonView = {

		let v = LHEmoticonView()
		v.frame.size = CGSizeMake(SCREENW, 216)
		v.backgroundColor = UIColor(patternImage: UIImage(named: "emoticon_keyboard_background")!)
		return v
	}()

	func setupUI() {

		view.addSubview(textView)
		textView.addSubview(imageColl)
		view.addSubview(toolBar)

		imageColl.addImageBlack = { [weak self] in
			self!.pushImagePickVC()
		}

		toolBar.snp_makeConstraints { (make) -> Void in
			make.leading.trailing.bottom.equalTo(view)
			make.height.equalTo(44)
		}

		textView.snp_makeConstraints { (make) -> Void in

			make.edges.equalTo(UIEdgeInsetsZero)
		}

		imageColl.snp_makeConstraints { (make) -> Void in
			make.top.equalTo(textView).offset(100)
			make.leading.equalTo(textView).offset(5)
			make.width.height.equalTo(textView.snp_width).offset(-2 * 5)
		}

		navigationItem.titleView = titelLbl

		navigationItem.leftBarButtonItem = UIBarButtonItem(title: "返回", target: self, action: "pop")

		navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBtn)
		navigationItem.rightBarButtonItem?.enabled = false
	}
	func send() {
	}

	private lazy var toolBar : LHStackView = {

		let stackV = LHStackView(frame: CGRectZero)
		stackV.distribution = .FillEqually
		stackV.delegeBtn = self
		return stackV
	}()

	@objc private func pop() {

		self.textView.resignFirstResponder()

		dismissViewControllerAnimated(true, completion: nil)
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	private func pushImagePickVC() {

		let VC = UIImagePickerController()
		VC.delegate = self
		presentViewController(VC, animated: true, completion: nil)
	}

	private lazy var textView : LHTextView = {

		let tV = LHTextView()

		tV.delegate = self

		tV.alwaysBounceVertical = true

		tV.placeholder = "呵呵哒呵呵哒····"
		tV.font = UIFont.systemFontOfSize(15.0)

		return tV
	}()

	private lazy var imageColl : LHImageCollection = {

		let imageC = LHImageCollection()

		imageC.hidden = true

		return imageC
	}()

	private lazy var rightBtn : UIButton = {

		let btn = UIButton()

		btn.setTitle("发送", forState: UIControlState.Normal)
		btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Disabled)
		btn.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Disabled)
		btn.setBackgroundImage(UIImage(named: "common_button_orange"), forState: UIControlState.Normal)
		btn.setBackgroundImage(UIImage(named: "common_button_orange_highlighted"), forState: UIControlState.Highlighted)
		btn.titleLabel?.font = UIFont.systemFontOfSize(14.0)
		btn.enabled = false
		btn.frame.size = CGSizeMake(45, 30)

		return btn
	}()

	private lazy var titelLbl : UILabel = {

		let lbl = UILabel()
		let name = (LHLoginAboutViewModel.sharedViewModel.userInfo?.screen_name)!
		let str = "发微博\n\(name)"
		lbl.numberOfLines = 2
		let rang = (str as NSString).rangeOfString("\(name)")
		let attr = NSMutableAttributedString(string: str)
		attr.addAttributes([NSFontAttributeName: UIFont.systemFontOfSize(15), NSForegroundColorAttributeName: UIColor.lightGrayColor()], range: rang)
		lbl.textAlignment = .Center
		lbl.attributedText = attr
		lbl.sizeToFit()
		return lbl
	}()
}

extension LHSendController : UITextViewDelegate, LHStackViewDelegetWithBtn, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

	func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {

		imageColl.addImage(image)

		picker.dismissViewControllerAnimated(true, completion: nil)
	}

	func textViewDidChange(textView: UITextView) {

		navigationItem.rightBarButtonItem?.enabled = textView.hasText()
	}

	func scrollViewWillBeginDragging(scrollView: UIScrollView) {

		textView.resignFirstResponder()
	}

	func stackViewBtnClick(type: LHToolBarButtonType) {

		switch type {

		case .Pic:
//			print("图片")
			pushImagePickVC()
		case .Trend:
			print("图片")
		case .Emoticon:
//			print("表情")
			emoticonBtnClick()
		case .Mention:
			print("图片")
		case .Add:
			print("图片")
		}
	}
}
