//
//  LHDiscoverController.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/18.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHDiscoverController: LHBaseTableController {
	
	
	private lazy var searchBar : LHSearchView = {
		
		let searchView: LHSearchView = LHSearchView.searchView()
		searchView.frame.size.width = UIScreen.mainScreen().bounds.size.width
		return searchView
	}()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		
		if loading {
			
            setupUI()
			
		} else {
			
			unView.setupInfo("登录后，最新、最热微博尽在掌握，不再会与实事潮流擦肩而过", imageName: "visitordiscover_image_message")
		}
		
		// Do any additional setup after loading the view.
	}
	func setupUI() {
		self.navigationItem.titleView = searchBar
		
		searchBar.searchBtnClickBlack = {(keyWord: String) -> () in
			
			print(keyWord)
			
		}
		
	}
	
	
//    override func viewWillDisappear(animated: Bool) {
//        super.viewWillDisappear(animated)
//
//        searchBar.searchField.endEditing(true)
//
//
//    }
	
	
}
