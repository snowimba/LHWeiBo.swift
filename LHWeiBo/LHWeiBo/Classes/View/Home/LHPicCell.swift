//
//  LHPicCell.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/24.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit
import SDWebImage
class LHPicCell: UICollectionViewCell {

	var picM : LHPicM? {

		didSet {

//            print(picM!.thumbnail_pic!)

			imageV.sd_setImageWithURL(NSURL(string: picM!.thumbnail_pic!), placeholderImage: UIImage(named: "timeline_image_placeholder"))
		}
	}

	override init(frame: CGRect) {
		super.init(frame: frame)

		setupUI()
	}

	private func setupUI() {

		contentView.addSubview(imageV)

		imageV.snp_makeConstraints { (make) -> Void in
			make.edges.equalTo(UIEdgeInsetsZero)
		}
	}

	lazy var imageV : UIImageView = {

		let imageP = UIImageView()
		imageP.contentMode = UIViewContentMode.ScaleAspectFill
		imageP.clipsToBounds = true
		return imageP
	}()

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
