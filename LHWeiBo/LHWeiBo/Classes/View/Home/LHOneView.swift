//
//  LHOneView.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/22.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit
import SnapKit
import MLLabel
class LHOneView: UIView {

	var usersViewModel : LHStatuseCellViewModel? {

		didSet {

			iconView.sd_setImageWithURL(NSURL(string: usersViewModel?.status?.user!.profile_image_url ?? ""), placeholderImage: UIImage(named: "avator_default"))
			nameLbl.text = usersViewModel?.status?.user?.screen_name ?? ""

			vipView.image = usersViewModel?.vipViewImage

			verifiedView.image = usersViewModel?.verifiedImage

			timeLbl.text = usersViewModel?.created_at ?? "未知"
			descLbl.text = usersViewModel?.source ?? "未知"
			contentLbl.text = usersViewModel?.status?.text ?? ""

			self.constr?.uninstall()
			if let pic = usersViewModel?.status?.pic_urls where pic.count > 0 {

				self.picView.hidden = false
//				constr?.uninstall()
				self.snp_makeConstraints { (make) -> Void in

					self.constr = make.bottom.equalTo(picView).offset(MARGIN).constraint
				}
				// self.twoView.usersViewModel = status

				self.picView.picArr = pic
			} else {

				self.picView.hidden = true
//				constr?.uninstall()
				self.snp_makeConstraints { (make) -> Void in

					self.constr = make.bottom.equalTo(contentLbl).offset(MARGIN).constraint
				}

				// self.twoView.usersViewModel = nil
			}
		}
	}

	override init(frame: CGRect) {
		super.init(frame: frame)
		setupUI()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	private var constr : Constraint?
	private func setupUI() {

		backgroundColor = UIColor.whiteColor()
		addSubview(iconView)
		addSubview(nameLbl)
		addSubview(vipView)
		addSubview(verifiedView)
		addSubview(timeLbl)
		addSubview(descLbl)
		addSubview(contentLbl)
		addSubview(picView)

		iconView.snp_makeConstraints { (make) -> Void in
			make.top.equalTo(self).offset(MARGIN)
			make.leading.equalTo(self).offset(MARGIN)
			make.size.equalTo(CGSizeMake(48, 48))
		}

		nameLbl.snp_makeConstraints { (make) -> Void in
			make.top.equalTo(iconView).offset(3)
			make.leading.equalTo(iconView.snp_trailing).offset(MARGIN)
		}

		vipView.snp_makeConstraints { (make) -> Void in
			make.centerY.equalTo(nameLbl)
			make.leading.equalTo(nameLbl.snp_trailing).offset(MARGIN)
		}

		verifiedView.snp_makeConstraints { (make) -> Void in
			make.right.equalTo(iconView.snp_right)
			make.bottom.equalTo(iconView.snp_bottom)
		}
		timeLbl.snp_makeConstraints { (make) -> Void in
			make.bottom.equalTo(iconView).offset(-3)
			make.leading.equalTo(nameLbl)
		}
		descLbl.snp_makeConstraints { (make) -> Void in
			make.centerY.equalTo(timeLbl)
			make.leading.equalTo(timeLbl.snp_trailing).offset(MARGIN)
		}

		contentLbl.snp_makeConstraints { (make) -> Void in
			make.top.equalTo(iconView.snp_bottom).offset(MARGIN)
			make.leading.equalTo(self).offset(MARGIN)
			make.trailing.equalTo(self).offset(-MARGIN)
		}

		picView.snp_makeConstraints { (make) -> Void in
			make.leading.equalTo(contentLbl)
			make.top.equalTo(contentLbl.snp_bottom).offset(MARGIN)
		}

		self.snp_makeConstraints { (make) -> Void in
			self.constr = make.bottom.equalTo(picView).offset(MARGIN).constraint
		}
	}

	private lazy var iconView : UIImageView = {

		let imageV = UIImageView(image: UIImage(named: "avator_default"))

		imageV.layer.cornerRadius = 24
		imageV.layer.borderWidth = 2
		imageV.layer.borderColor = UIColor.orangeColor().CGColor

		imageV.layer.masksToBounds = true

		return imageV
	}()

	private lazy var nameLbl : UILabel = {

		let lbl = UILabel()

		lbl.font = UIFont.systemFontOfSize(13.0)

		return lbl
	}()

	private lazy var vipView : UIImageView = {

		let imageV = UIImageView(image: UIImage(named: "common_icon_membership"))

//        imageV.layer.cornerRadius = 24

//        imageV.layer.masksToBounds = true
//		imageV.hidden = true

		return imageV
	}()

	private lazy var verifiedView : UIImageView = {

		let imageV = UIImageView(image: UIImage(named: "avatar_vip"))

		// imageV.layer.cornerRadius = 24

		// imageV.layer.masksToBounds = true
//		imageV.hidden = true

		return imageV
	}()

	private lazy var descLbl : UILabel = {

		let lbl = UILabel()

		lbl.font = UIFont.systemFontOfSize(13.0)

		lbl.textColor = UIColor.darkGrayColor()

		return lbl
	}()

	private lazy var timeLbl : UILabel = {

		let lbl = UILabel()

		lbl.font = UIFont.systemFontOfSize(13.0)

		lbl.textColor = UIColor.orangeColor()

		return lbl
	}()

	private lazy var contentLbl : MLLinkLabel = {

		let lbl = MLLinkLabel()
        lbl.dataDetectorTypes = [.UserHandle,.URL,.Hashtag]

		lbl.font = UIFont.systemFontOfSize(14.0)

//        lbl.textColor = UIColor.orangeColor()
		lbl.numberOfLines = 0

		return lbl
	}()

	private lazy var picView : LHPicCollectionView = {

		let pic = LHPicCollectionView()
		pic.backgroundColor = UIColor(white: 245 / 255, alpha: 1)

		return pic
	}()
}
