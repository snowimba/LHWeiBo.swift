//
//  LHTwoView.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/22.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit
import SnapKit
import MLLabel
class LHTwoView: UIView {

	var usersViewModel : LHStatuseCellViewModel? {

		didSet {

			contentLbl.text = usersViewModel?.retweetText
			self.constr?.uninstall()

			if let pic = usersViewModel?.status?.retweeted_status?.pic_urls where pic.count > 0 {

				self.picView.hidden = false
                self.picView.picArr = pic
				self.snp_makeConstraints { (make) -> Void in

					self.constr = make.bottom.equalTo(picView).offset(MARGIN).constraint
				}
//				self.twoView.usersViewModel = status

			} else {

				self.picView.hidden = true
//				constr?.uninstall()
				self.snp_makeConstraints { (make) -> Void in

					self.constr = make.bottom.equalTo(contentLbl).offset(MARGIN).constraint
				}

				// self.twoView.usersViewModel = nil
			}
		}
	}
	private var constr : Constraint?
	override init(frame: CGRect) {
		super.init(frame: frame)

		backgroundColor = UIColor(white: 245 / 255, alpha: 1)
		setupUI()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func setupUI() {

		addSubview(contentLbl)
		addSubview(picView)

		contentLbl.snp_makeConstraints { (make) -> Void in
			make.leading.equalTo(self).offset(MARGIN)
			make.trailing.equalTo(self).offset(-MARGIN)
			make.top.equalTo(self).offset(MARGIN)
		}

		picView.snp_makeConstraints { (make) -> Void in
			make.leading.equalTo(contentLbl)
			make.top.equalTo(contentLbl.snp_bottom).offset(MARGIN)
		}

		snp_makeConstraints { (make) -> Void in
			self.constr = make.bottom.equalTo(picView).offset(MARGIN).constraint
		}
	}

	private lazy var contentLbl : MLLinkLabel = {

		let lbl = MLLinkLabel()
        lbl.dataDetectorTypes = [.UserHandle,.URL,.Hashtag]

		lbl.font = UIFont.systemFontOfSize(14.0)
		lbl.numberOfLines = 0

		return lbl
	}()

	private lazy var picView : LHPicCollectionView = {

		let pic = LHPicCollectionView()

		pic.backgroundColor = UIColor(white: 245 / 255, alpha: 1)

		return pic
	}()
	/*
	 // Only override drawRect: if you perform custom drawing.
	 // An empty implementation adversely affects performance during animation.
	 override func drawRect(rect: CGRect) {
	 // Drawing code
	 }
	 */
}
