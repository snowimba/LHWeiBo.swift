//
//  LHHomeController.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/18.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit
import SVProgressHUD
class LHHomeController: LHBaseTableController {

	let reuseID = "homeCell"

	override func viewDidLoad() {
		super.viewDidLoad()

		if loading {

			setupUI()
		} else {

			unView.setupInfo(nil, imageName: nil)
		}
	}

//	private lazy var page : Int = 1

	let refresh : YiRefreshHeader = YiRefreshHeader()
	let refreshF : YiRefreshFooter = YiRefreshFooter()
//	var page : Int = 0

	func setupUI() {

		navigationItem.leftBarButtonItem = UIBarButtonItem(imageName: "navigationbar_friendsearch", target: self, action: "friendsearch")

		navigationItem.rightBarButtonItem = UIBarButtonItem(imageName: "navigationbar_pop", target: self, action: "friendsearch")

		tableView.registerClass(LHStatuseCell.self, forCellReuseIdentifier: reuseID)

		tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.rowHeight = 300

		tableView.estimatedRowHeight = 200

//		tableView.bouncesZoom = false

		tableView.backgroundColor = UIColor(white: 242 / 255, alpha: 1)

		tableView.separatorStyle = .None

//		LHStatuseAboutViewModel.sharedStatusVM.loadStatusData { (isSucced) -> () in
//
//			if isSucced {
//
//				self.tableView.reloadData()
//			}
//		}
//		var num : Int = 0

		navigationController?.navigationBar.translucent = false

		refresh.scrollView = tableView

		refresh.header()

		refresh.beginRefreshingBlock = {

			LHStatuseAboutViewModel.sharedStatusVM.loadStatusData { [weak self](isSucced) -> () in

				if isSucced {

					self?.tableView.reloadData()
//					self?.refresh.endRefreshing()
					// self?.page = 1
				} else {

					SVProgressHUD.showErrorWithStatus("请求失败!")
				}
				self?.refresh.endRefreshing()
			}
		}

		refresh.beginRefreshing()
        
        
        

		refreshF.scrollView = tableView

		refreshF.footer()

		refreshF.beginRefreshingBlock = {
//			num = num + 1
//			self.page = self.page + 1
//            print(self.page)
			LHStatuseAboutViewModel.sharedStatusVM.loadMoreStatusData { [weak self](isSucced) -> () in

				if isSucced {

					self?.tableView.reloadData()
//					self?.refreshF.endRefreshing()
				} else {

					SVProgressHUD.showErrorWithStatus("请求失败!")
				}
				self?.refreshF.endRefreshing()
			}
		}
	}

	func friendsearch() {

		navigationController?.pushViewController(LHNextViewController(), animated: true)
	}
}

extension LHHomeController {

	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return LHStatuseAboutViewModel.sharedStatusVM.statusesArr?.count ?? 0
	}

	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		let cell = tableView.dequeueReusableCellWithIdentifier(reuseID, forIndexPath: indexPath) as! LHStatuseCell

		cell.status = LHStatuseAboutViewModel.sharedStatusVM.statusesArr![indexPath.row]

		return cell
	}
}
