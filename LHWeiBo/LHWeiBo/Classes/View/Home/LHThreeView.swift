//
//  LHThreeView.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/22.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHThreeView: UIView {

	var usersViewModel : LHStatuseCellViewModel? {

		didSet {

			if let retweetNum = usersViewModel?.retweet {

				retweetBtn.setTitle(retweetNum, forState: UIControlState.Normal)
			}
            
            if let commentNum = usersViewModel?.comment {
                
                commentBtn.setTitle(commentNum, forState: UIControlState.Normal)
            }
            
            if let unlikeNum = usersViewModel?.unlike {
                
                
                unlikeBtn.setTitle(unlikeNum, forState: UIControlState.Normal)
            }

			

		}
	}
	var retweetBtn : UIButton!
	var commentBtn : UIButton!
	var unlikeBtn : UIButton!
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupUI()
	}

	var sp1 : UIView!
	var sp2 : UIView!

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func setupUI() {

		retweetBtn = getBtn("timeline_icon_retweet", titel: "转发")
		commentBtn = getBtn("timeline_icon_comment", titel: "收藏")
		unlikeBtn = getBtn("timeline_icon_unlike", titel: "赞")

		sp1 = getSpImageView()
		sp2 = getSpImageView()

		addSubview(sp1)
		addSubview(sp2)

		retweetBtn.snp_makeConstraints(closure: { (make) -> Void in
				make.leading.equalTo(self)
//				make.trailing.equalTo(self).offset(-MARGIN)
				make.top.equalTo(self)
				make.bottom.equalTo(self)
				make.width.equalTo(commentBtn)
			})

		commentBtn.snp_makeConstraints(closure: { (make) -> Void in
				make.leading.equalTo(retweetBtn.snp_trailing)
				// make.trailing.equalTo(self).offset(-MARGIN)
				make.top.equalTo(self)
				make.bottom.equalTo(self)
				make.width.equalTo(unlikeBtn)
			})

		unlikeBtn.snp_makeConstraints(closure: { (make) -> Void in
				make.leading.equalTo(commentBtn.snp_trailing)
				// make.trailing.equalTo(self).offset(-MARGIN)
				make.top.equalTo(self)
				make.bottom.equalTo(self)
				make.trailing.equalTo(self)
			})

		sp1.snp_makeConstraints(closure: { (make) -> Void in
				make.centerY.equalTo(self)
				make.trailing.equalTo(commentBtn.snp_leading)
			})
		sp2.snp_makeConstraints(closure: { (make) -> Void in
				make.centerY.equalTo(self)
				make.trailing.equalTo(unlikeBtn.snp_leading)
			})
	}

	private func getSpImageView() -> UIView {

		let sp = UIImageView(image: UIImage(named: "timeline_card_bottom_line_highlighted"))

		return sp
	}

	private func getBtn(imageName: String, titel: String) -> UIButton {

		let btn : UIButton = UIButton()

		btn.setTitle(titel, forState: UIControlState.Normal)
		btn.setImage(UIImage(named: imageName), forState: UIControlState.Normal)
		btn.setBackgroundImage(UIImage(named: "timeline_card_bottom_background"), forState: UIControlState.Normal)
		btn.titleLabel?.font = UIFont.systemFontOfSize(13.0)
		btn.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
		btn.setBackgroundImage(UIImage(named: "timeline_card_bottom_background_highlighted"), forState: UIControlState.Highlighted)

		addSubview(btn)

		return btn
	}
	/*
	 // Only override drawRect: if you perform custom drawing.
	 // An empty implementation adversely affects performance during animation.
	 override func drawRect(rect: CGRect) {
	 // Drawing code
	 }
	 */
}
