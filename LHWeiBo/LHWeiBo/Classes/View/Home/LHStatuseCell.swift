//
//  LHStatuseCell.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/22.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit
import SnapKit
class LHStatuseCell: UITableViewCell {

	var status : LHStatuseCellViewModel? {

		didSet {

			self.oneView.usersViewModel = status
			self.threeView.usersViewModel = status

			if status?.status?.retweeted_status != nil {

				constr?.uninstall()
				threeView.snp_makeConstraints { (make) -> Void in

					constr = make.top.equalTo(twoView.snp_bottom).constraint
				}
				self.twoView.usersViewModel = status
				self.twoView.hidden = false
			} else {

				constr?.uninstall()
				threeView.snp_makeConstraints { (make) -> Void in

					constr = make.top.equalTo(oneView.snp_bottom).constraint
				}

				self.twoView.hidden = true
//				self.twoView.usersViewModel = nil
			}
		}
	}

	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)

		setupUI()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private lazy var oneView : LHOneView = LHOneView()

	private lazy var threeView : LHThreeView = LHThreeView()

	private lazy var twoView : LHTwoView = LHTwoView()

	private var constr : Constraint?

	private func setupUI() {

		selectionStyle = .None
		contentView.backgroundColor = UIColor(white: 240 / 255, alpha: 1)

		contentView.addSubview(oneView)
		contentView.addSubview(threeView)
		contentView.addSubview(twoView)

		oneView.snp_makeConstraints { (make) -> Void in

			make.top.equalTo(contentView).offset(MARGIN)
			make.trailing.equalTo(contentView)
			make.leading.equalTo(contentView)
		}

		twoView.snp_makeConstraints { (make) -> Void in

			make.top.equalTo(oneView.snp_bottom)
			make.trailing.equalTo(contentView)
			make.leading.equalTo(contentView)

		}

		threeView.snp_makeConstraints { (make) -> Void in

			self.constr = make.top.equalTo(twoView.snp_bottom).constraint
			make.trailing.equalTo(contentView)
			make.leading.equalTo(contentView)
			make.height.equalTo(36)
		}
		contentView.snp_makeConstraints { (make) -> Void in
			make.bottom.equalTo(threeView.snp_bottom)
			make.trailing.equalTo(self)
			make.leading.equalTo(self)
			make.top.equalTo(self)
		}
	}
}

/*

UIView.h> may also be helpful.
2016-01-24 18:22:32.756 LHWeiBo[14476:352026] Unable to simultaneously satisfy constraints.
Probably at least one of the constraints in the following list is one you don't want. Try this: (1) look at each constraint and try to figure out which you don't expect; (2) find the code that added the unwanted constraint or constraints and fix it. (Note: If you're seeing NSAutoresizingMaskLayoutConstraints that you don't understand, refer to the documentation for the UIView property translatesAutoresizingMaskIntoConstraints)
(
"<SnapKit.LayoutConstraint:0x7fe80a49a550@/Users/snowimba/Desktop/\U9879\U76ee/newWeibo/LHWeiBo/LHWeiBo/Classes/View/Home/LHPicCollectionView.swift#23 LHWeiBo.LHPicCollectionView:0x7fe80c078600.width == 116.0>",
"<SnapKit.LayoutConstraint:0x7fe80ca01ee0@/Users/snowimba/Desktop/\U9879\U76ee/newWeibo/LHWeiBo/LHWeiBo/Classes/View/Home/LHPicCollectionView.swift#23 LHWeiBo.LHPicCollectionView:0x7fe80c078600.width == 237.0>"
)

Will attempt to recover by breaking constraint
<SnapKit.LayoutConstraint:0x7fe80ca01ee0@/Users/snowimba/Desktop/项目/newWeibo/LHWeiBo/LHWeiBo/Classes/View/Home/LHPicCollectionView.swift#23 LHWeiBo.LHPicCollectionView:0x7fe80c078600.width == 237.0>

Make a symbolic breakpoint at UIViewAlertForUnsatisfiableConstraints to catch this in the debugger.
The methods in the UIConstraintBasedLayoutDebugging category on UIView listed in <UIKit/UIView.h> may also be helpful.

*/




