//
//  LHBigPicCollection.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/26.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHBigPicCollection: UICollectionView {

	let itemID = "BIGPIC"
	var indexP : NSIndexPath? {

		didSet {

			numLbl.text = "\(indexP!.item+1)/\(picArr!.count)"

			self.contentOffset = CGPointMake(SCREENW * CGFloat((indexP?.item)!), 0)
		}
	}
	var picArr : [LHPicM]? {

		didSet {

			reloadData()
//			 print("reloadData")
//            selectItemAtIndexPath(indexP, animated: false, scrollPosition: UICollectionViewScrollPosition.None)
		}
	}

	override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
		super.init(frame: frame, collectionViewLayout: UICollectionViewFlowLayout())

		self.registerClass(LHBigPicView.self, forCellWithReuseIdentifier: itemID)

		dataSource = self
		delegate = self
		pagingEnabled = true

//		self.scrollsToTop = false
		// self.collectionViewLayout

		NSNotificationCenter.defaultCenter().addObserver(self, selector: "popBlackNoti:", name: "PopBlackNoti", object: nil)
		setupUI()
	}

	deinit {

		NSNotificationCenter.defaultCenter().removeObserver(self)
	}

	@objc private func popBlackNoti(noti: NSNotification) {

		let vie = noti.object as! UIView
		let cell = vie.superview as! LHBigPicView

		let indexP = self.indexPathForCell(cell)

		let picM = picArr![indexP!.item]

		popBlack(cell, picM: picM)
	}

	private func setupUI() {
		let layout = collectionViewLayout as! UICollectionViewFlowLayout

		layout.minimumLineSpacing = 0
		layout.minimumInteritemSpacing = 0
		layout.sectionInset = UIEdgeInsetsZero
		layout.scrollDirection = .Horizontal
		layout.itemSize = CGSize(width: SCREENW, height: SCREENH)
		let window = UIApplication.sharedApplication().keyWindow!
		window.addSubview(numLbl)
		window.addSubview(saveLbl)

		numLbl.snp_makeConstraints { (make) -> Void in
			make.centerX.equalTo(window)
			make.top.equalTo(window).offset(30)
		}
		saveLbl.snp_makeConstraints { (make) -> Void in
			make.leading.equalTo(window).offset(30)
			make.bottom.equalTo(window).offset(-30)
		}
		window.bringSubviewToFront(numLbl)
		window.bringSubviewToFront(saveLbl)
	}

	lazy var numLbl : UILabel = {

		let lbl = UILabel()

		lbl.layer.cornerRadius = 3

		lbl.layer.masksToBounds = true

		lbl.backgroundColor = UIColor(white: 44 / 255, alpha: 1)

		lbl.font = UIFont.boldSystemFontOfSize(18.0)
		lbl.textColor = UIColor.whiteColor()
		return lbl
	}()

	lazy var saveLbl : UILabel = {

		let lbl = UILabel()

		lbl.layer.cornerRadius = 3

		lbl.text = "保存"

		lbl.font = UIFont.systemFontOfSize(18.0)
		lbl.textColor = UIColor.whiteColor()
		lbl.layer.masksToBounds = true

		lbl.backgroundColor = UIColor(white: 44 / 255, alpha: 1)

		return lbl
	}()

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

extension LHBigPicCollection : UICollectionViewDataSource {

	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

		return picArr?.count ?? 0
	}

	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

		let cell = dequeueReusableCellWithReuseIdentifier(itemID, forIndexPath: indexPath) as! LHBigPicView

		cell.picM = picArr![indexPath.item]
//		numLbl.text = "\(indexPath.item+1)/\(picArr!.count)"

		return cell
	}
}

extension LHBigPicCollection : UICollectionViewDelegate {

	func scrollViewDidScroll(scrollView: UIScrollView) {
		let index = Int(scrollView.contentOffset.x / SCREENW + 0.5 + 1)
		numLbl.text = "\(index)/\(picArr!.count)"
	}

//	func scrollViewWillBeginDecelerating(scrollView: UIScrollView) {
//		let index = Int(scrollView.contentOffset.x / SCREENW + 0.5)
//
//		let indexP = NSIndexPath(forItem: index, inSection: 0)
//
//		let cell = self.cellForItemAtIndexPath(indexP) as! LHBigPicView
//
//		let image = cell.bigPic
//
//		UIView.animateWithDuration(0.25) { () -> Void in
//
//			image.frame.size.width = SCREENW
//		}
//	}

	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//
////		print("888888")
//		let window = UIApplication.sharedApplication().keyWindow!
		let cell = collectionView.cellForItemAtIndexPath(indexPath) as! LHBigPicView
		let picM = picArr![indexPath.item]

		popBlack(cell, picM: picM)
//		let imageN = UIImageView()
//
//		window.addSubview(imageN)
//
//		imageN.image = cell.bigPic.image
//
//		imageN.frame.size = cell.bigPic.frame.size
//		imageN.center = CGPointMake(SCREENW * 0.5, SCREENH * 0.5)
//
//		cell.hidden = true
//
//		UIView.animateWithDuration(0.5, animations: { () -> Void in
//
//			imageN.frame = picM.rect!
//
//			self.alpha = 0
//		}) { (_) -> Void in
//
//			imageN.removeFromSuperview()
//			self.removeFromSuperview()
//			self.numLbl.removeFromSuperview()
//			self.saveLbl.removeFromSuperview()
//		}
	}

	func popBlack(cell: LHBigPicView, picM: LHPicM) {

		let window = UIApplication.sharedApplication().keyWindow!
//		let cell = collectionView.cellForItemAtIndexPath(indexPath) as! LHBigPicView
//		let picM = picArr![indexPath.item]
		let imageN = UIImageView()

		window.addSubview(imageN)

		imageN.image = cell.bigPic.image

		imageN.frame.size = cell.bigPic.frame.size
		imageN.center = CGPointMake(SCREENW * 0.5, SCREENH * 0.5)

		cell.hidden = true

		UIView.animateWithDuration(0.25, animations: { () -> Void in

			imageN.frame = picM.rect!
			self.numLbl.alpha = 0
			self.saveLbl.alpha = 0
			self.alpha = 0
		}) { (_) -> Void in

			imageN.removeFromSuperview()
			self.removeFromSuperview()
			self.numLbl.removeFromSuperview()
			self.saveLbl.removeFromSuperview()
		}
	}
}
