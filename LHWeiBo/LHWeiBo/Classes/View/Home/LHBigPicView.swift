//
//  LHBigPicView.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/26.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit
import SDWebImage
class LHBigPicView: UICollectionViewCell {

	var picM : LHPicM? {

		didSet {

			guard let imageO = SDWebImageManager.sharedManager().imageCache.imageFromDiskCacheForKey(picM!.thumbnail_pic!) else {

				return
			}
			var str : String = picM!.thumbnail_pic!

			if (str.rangeOfString("thumbnail") != nil) {

				str = str.stringByReplacingOccurrencesOfString("thumbnail", withString: "bmiddle")
			}

			bigPic.sd_setImageWithURL(NSURL(string: str), placeholderImage: imageO)

			bigPic.frame.size.width = SCREENW
			bigPic.frame.size.height = SCREENW * imageO.size.height / imageO.size.width
			bigPic.frame.origin = CGPointMake(0, 0)

			if bigPic.frame.size.height > SCREENH {

				scroll.snp_updateConstraints { (make) -> Void in
					make.size.equalTo(self.frame.size)
//					make.height.equalTo(self.frame.size.height)
//					make.width.equalTo(self.frame.size.width).offset(-20)
				}
				scroll.contentSize = CGSizeMake(0, bigPic.frame.size.height)
			} else {

				scroll.snp_updateConstraints { (make) -> Void in
					make.size.equalTo(bigPic.frame.size)
//					make.height.equalTo(bigPic.frame.size.height)
//					make.width.equalTo(bigPic.frame.size.width).offset(20)
				}
				scroll.contentSize = CGSizeMake(0, 0)
			}
		}
	}

	override init(frame: CGRect) {
		super.init(frame: frame)

		setupUI()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func setupUI() {
		backgroundColor = UIColor.clearColor()
		contentView.backgroundColor = UIColor.clearColor()
		contentView.addSubview(scroll)
		scroll.addSubview(bigPic)
		bigPic.userInteractionEnabled = true
//		contentView.addSubview(numLbl)
//		contentView.addSubview(saveLbl)
		scroll.backgroundColor = UIColor.clearColor()

		scroll.snp_makeConstraints { (make) -> Void in
			make.center.equalTo(self)
		}
//		numLbl.snp_makeConstraints { (make) -> Void in
//			make.centerX.equalTo(self)
//			make.top.equalTo(self).offset(30)
//		}
//		saveLbl.snp_makeConstraints { (make) -> Void in
//			make.leading.equalTo(self).offset(30)
//			make.bottom.equalTo(self).offset(-30)
//		}
	}

//	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//
//		finishBack?()
//
////		self.removeFromSuperview()
//	}

//	var finishBack : (() -> ())?

	lazy var bigPic : UIImageView = UIImageView()

	private lazy var scroll : LHscrollV = LHscrollV()

//	lazy var numLbl : UILabel = {
//
//		let lbl = UILabel()
//
//		lbl.layer.cornerRadius = 5
//
//		lbl.layer.masksToBounds = true
//
//		lbl.backgroundColor = UIColor(white: 44 / 255, alpha: 0.8)
//
//		lbl.font = UIFont.systemFontOfSize(18.0)
//		lbl.textColor = UIColor.whiteColor()
//		return lbl
//	}()
//
//	lazy var saveLbl : UILabel = {
//
//		let lbl = UILabel()
//
//		lbl.layer.cornerRadius = 5
//
//		lbl.text = "保存"
//
//		lbl.font = UIFont.systemFontOfSize(18.0)
//		lbl.textColor = UIColor.whiteColor()
//		lbl.layer.masksToBounds = true
//
//		lbl.backgroundColor = UIColor(white: 44 / 255, alpha: 0.8)
//
//		return lbl
//	}()
}

class LHscrollV : UIScrollView {

	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {

//		print("*****")
		NSNotificationCenter.defaultCenter().postNotificationName("PopBlackNoti", object: self.superview)
	}

//	override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
//
////        print(self.dragging)
//
//		if event?.type == .Touches {
//
//			return nil
//		}
//
//		return super.hitTest(point, withEvent: event)
//	}
}
