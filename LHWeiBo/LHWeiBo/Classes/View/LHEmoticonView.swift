//
//  LHEmoticonView.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/28.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHEmoticonView: UIView {
	let itemID = "EonticID"
	override init(frame: CGRect) {
		super.init(frame: frame)

		setupUI()

		NSNotificationCenter.defaultCenter().addObserver(self, selector: "changeCell:", name: "changeCell", object: nil)
	}

	deinit {

		NSNotificationCenter.defaultCenter().removeObserver(self)
	}

	@objc private func changeCell(noti: NSNotification) {

		let index = noti.object as! Int

		emoticColl.scrollToItemAtIndexPath(NSIndexPath(forItem: 0, inSection: index), atScrollPosition: UICollectionViewScrollPosition.Left, animated: false)

		pageVC.numberOfPages = LHEmoticViewModel.sharedViewModel.allEmotics[index].count == 1 ? 0 : LHEmoticViewModel.sharedViewModel.allEmotics[index].count
		pageVC.currentPage = 0
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func setupUI() {

		addSubview(emoticColl)
		addSubview(toolBar)
		addSubview(pageVC)

		pageVC.snp_makeConstraints { (make) -> Void in
			make.leading.trailing.equalTo(self)
			make.bottom.equalTo(toolBar.snp_top)
			make.height.equalTo(37)
		}

		emoticColl.snp_makeConstraints { (make) -> Void in
			make.top.leading.trailing.equalTo(self)
			make.bottom.equalTo(self).offset(-37 - 20)
		}

		toolBar.snp_makeConstraints { (make) -> Void in
			make.leading.trailing.bottom.equalTo(self)
			make.height.equalTo(37)
		}
	}
	override func layoutSubviews() {
		super.layoutSubviews()

		let layout = emoticColl.collectionViewLayout as! UICollectionViewFlowLayout
		layout.minimumLineSpacing = 0
		layout.minimumInteritemSpacing = 0
		layout.scrollDirection = .Horizontal
		layout.sectionInset = UIEdgeInsetsZero
		layout.itemSize = emoticColl.frame.size
	}

	private lazy var pageVC : UIPageControl = {
		let pageC = UIPageControl()

		pageC.currentPageIndicatorTintColor = UIColor.orangeColor()
		pageC.pageIndicatorTintColor = UIColor.whiteColor()
		pageC.currentPage = 0
		pageC.setValue(UIImage(named: "compose_keyboard_dot_selected"), forKey: "_currentPageImage")
		pageC.setValue(UIImage(named: "compose_keyboard_dot_normal"), forKey: "_pageImage")

		return pageC
	}()

	private lazy var emoticColl : UICollectionView = {

		let coll = UICollectionView(frame: CGRectZero, collectionViewLayout: UICollectionViewFlowLayout())
		coll.backgroundColor = UIColor.clearColor()
		coll.delegate = self
		coll.dataSource = self
		coll.bounces = false
		coll.pagingEnabled = true
		coll.registerClass(LHEonticCell.self, forCellWithReuseIdentifier: self.itemID)

		coll.showsHorizontalScrollIndicator = false

		return coll
	}()

	private lazy var toolBar : LHEmoticTool = {

		let stackV = LHEmoticTool(frame: CGRectZero)
		stackV.distribution = .FillEqually
		return stackV
	}()
}

extension LHEmoticonView : UICollectionViewDataSource, UICollectionViewDelegate {

	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return LHEmoticViewModel.sharedViewModel.allEmotics.count
	}

	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

		return LHEmoticViewModel.sharedViewModel.allEmotics[section].count
	}

	func scrollViewDidScroll(scrollView: UIScrollView) {

		let cells = emoticColl.visibleCells()

		if cells.count == 2 {

			let firstCell = cells.first
			let lastCell = cells.last

			let cell = abs(firstCell!.frame.origin.x - scrollView.contentOffset.x) < abs(lastCell!.frame.origin.x - scrollView.contentOffset.x) ? firstCell : lastCell
			let index = emoticColl.indexPathForCell(cell!)

			toolBar.shouldClickBtn((index?.section)!)

			pageVC.numberOfPages = LHEmoticViewModel.sharedViewModel.allEmotics[index!.section].count == 1 ? 0 : LHEmoticViewModel.sharedViewModel.allEmotics[index!.section].count
			pageVC.currentPage = (index?.item)!
		}
	}

	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = emoticColl.dequeueReusableCellWithReuseIdentifier(itemID, forIndexPath: indexPath) as! LHEonticCell

		cell.emotics = LHEmoticViewModel.sharedViewModel.allEmotics[indexPath.section] [indexPath.item]
		return cell
	}
}
