//
//  LHImageCollection.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/27.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHImageCollection: UICollectionView {

	let itemID = "IMAGEID"
	private lazy var imageArr : [UIImage] = [UIImage]()

	override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
		super.init(frame: frame, collectionViewLayout: UICollectionViewFlowLayout())

		registerClass(LHImageItem.self, forCellWithReuseIdentifier: itemID)
		delegate = self
		dataSource = self

		backgroundColor = UIColor.whiteColor()
		setupUI()
	}

	func addImage(image: UIImage) {
		imageArr.append(image)

		reloadData()
		hidden = false
	}

	var addImageBlack : (() -> ())?

	private func setupUI() {

		let layout = collectionViewLayout as! UICollectionViewFlowLayout

		layout.minimumLineSpacing = 5
		layout.minimumInteritemSpacing = 5
		let iamgeW = (SCREENW - 4 * 5) / 3
//        layout.sectionInset = UIEdgeInsetsZero
		layout.itemSize = CGSize(width: iamgeW, height: iamgeW)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

extension LHImageCollection : UICollectionViewDataSource, UICollectionViewDelegate {

	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

		return (imageArr.count > 0 && imageArr.count < 9) ? imageArr.count + 1: imageArr.count
	}

	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = dequeueReusableCellWithReuseIdentifier(itemID, forIndexPath: indexPath) as! LHImageItem

		if indexPath.item < imageArr.count {

			cell.image = imageArr[indexPath.item]
			cell.deleBtnBlack = { [weak self](cell: LHImageItem) -> () in

				let indexP = self!.indexPathForCell(cell)

				self!.imageArr.removeAtIndex((indexP?.item)!)

				self!.reloadData()
				self!.hidden = self!.imageArr.count == 0
			}
		} else {
			cell.image = nil
		}

		return cell
	}

	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {

//        print(indexPath.item)
		if indexPath.item == imageArr.count {

			addImageBlack?()
		}

		self.deselectItemAtIndexPath(indexPath, animated: false)
	}
}

class LHImageItem : UICollectionViewCell {

	var image : UIImage? {

		didSet {
			if image != nil {

				deleBtn.hidden = false
				imageV.image = image
				imageV.highlightedImage = nil
			} else {

				deleBtn.hidden = true
				imageV.image = UIImage(named: "compose_pic_add")
				imageV.highlightedImage = UIImage(named: "compose_pic_add_highlighted")
			}
		}
	}

	override init(frame: CGRect) {
		super.init(frame: frame)
		setupUI()
	}

	private func setupUI() {

		self.contentView.addSubview(imageV)
		self.contentView.addSubview(deleBtn)

		deleBtn.addTarget(self, action: "deleBtnClick", forControlEvents: UIControlEvents.TouchUpInside)

		imageV.snp_makeConstraints { (make) -> Void in
			make.edges.equalTo(UIEdgeInsetsZero)
		}
		deleBtn.snp_makeConstraints { (make) -> Void in
			make.top.trailing.equalTo(contentView)
		}
	}

	var deleBtnBlack: ((cell: LHImageItem) -> ())?

	@objc private func deleBtnClick() {

		deleBtnBlack?(cell: self)
	}

	private var imageV : UIImageView = UIImageView()

	private var deleBtn : UIButton = {

		let btn = UIButton()

		btn.setBackgroundImage(UIImage(named: "compose_photo_close"), forState: UIControlState.Normal)

		return btn
	}()

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
