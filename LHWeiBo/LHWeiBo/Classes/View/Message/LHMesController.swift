//
//  LHMesController.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/18.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHMesController: LHBaseTableController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
        if loading {
            
                        setupUI()
            
        } else {
            
            unView.setupInfo("登录后，别人评论你的微博，发给你的消息，都会在这里收到通知", imageName: "visitordiscover_image_message")
        }
		// Do any additional setup after loading the view.
//		setupUI()
	}
	
	func setupUI() {
		
		navigationItem.leftBarButtonItem = UIBarButtonItem(title: "群消息", target: self, action: "push")
		
		tabBarItem.badgeValue = "10"
		
	}
	
	func push() {
		
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	/*
	 // MARK: - Navigation

	 // In a storyboard-based application, you will often want to do a little preparation before navigation
	 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	 // Get the new view controller using segue.destinationViewController.
	 // Pass the selected object to the new view controller.
	 }
	 */
	
}
