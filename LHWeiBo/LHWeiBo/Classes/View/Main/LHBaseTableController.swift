//
//  LHBaseTableController.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/18.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit
import AFNetworking

class LHBaseTableController: UITableViewController {

	var loading : Bool = LHLoginAboutViewModel.sharedViewModel.isLogin

	lazy var unView : LHUnloadView = LHUnloadView()

	override func loadView() {

		if loading {
            
			super.loadView()
		} else {

			view = unView

			unView.registerBtn.addTarget(self, action: "registerBtnClick", forControlEvents: UIControlEvents.TouchUpInside)
			unView.loginBtn.addTarget(self, action: "loginBtnClick", forControlEvents: UIControlEvents.TouchUpInside)
			view.backgroundColor = UIColor(red: 237.0 / 255, green: 237.0 / 255, blue: 237.0 / 255, alpha: 1)

			navigationItem.leftBarButtonItem = UIBarButtonItem(title: "注册", target: self, action: "registerBtnClick")
			navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登陆", target: self, action: "loginBtnClick")
		}
	}

	func registerBtnClick() {

		let loginVC = LHLoginController()

		loginVC.type = "reg"

		presentViewController(LHNaController(rootViewController: loginVC), animated: true, completion: nil)
	}

	func loginBtnClick() {

		let loginVC = LHLoginController()

//		loginVC.parametersBlack = {(parameters : [String: AnyObject]) -> Void in
//
////			print(parameters)
////			https://api.weibo.com/oauth2/access_token
//
//			let manger = LHSessionManager.sessionManager
//
//
//            manger.request(.POST, URLString: "https://api.weibo.com/oauth2/access_token", parameters: parameters, finishBack: { (response, error) -> () in
//                print(response)
//                print(error)
//            })
//
//		}

//        w8899308@163.com

		loginVC.type = "login"

		presentViewController(UINavigationController(rootViewController: loginVC), animated: true, completion: nil)
	}
}
