//
//  LHTabBar.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/18.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHTabBar: UITabBar {
	
	
	private lazy var addBtn : UIButton = {
		
		let addBtn : UIButton = UIButton()
		
		addBtn.addTarget(self, action: "addBtnClick", forControlEvents: UIControlEvents.TouchUpInside)
		
		addBtn.setBackgroundImage(UIImage(named: "tabbar_compose_button"), forState: UIControlState.Normal)
		addBtn.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: UIControlState.Highlighted)
		addBtn.setImage(UIImage(named: "tabbar_compose_icon_add"), forState: UIControlState.Normal)
		addBtn.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted"), forState: UIControlState.Highlighted)
		addBtn.sizeToFit()
		return addBtn
	}()
	
	var addBtnClickBlack: (() -> ())?
	
	@objc private func addBtnClick() {
		
		addBtnClickBlack?()
		
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		setUp()
	}
	
	func setUp() {
		
		addSubview(addBtn)
        
        backgroundImage = UIImage(named: "tabbar_background")
		
		
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		addBtn.center = CGPoint(x: frame.width * 0.5, y: frame.height * 0.5)
		
		var index = 0
		
		let widthV = frame.width / 5
		
		for subView in self.subviews {
			
			if subView.isKindOfClass(NSClassFromString("UITabBarButton")!) {
				
				subView.frame.size.width = widthV
				
				subView.frame.origin.x = CGFloat(index) * widthV
				
				index++
				
				if index == 2 {
					index++
				}
				
			}
			
			
		}
		
	}
	
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	/*
	 // Only override drawRect: if you perform custom drawing.
	 // An empty implementation adversely affects performance during animation.
	 override func drawRect(rect: CGRect) {
	 // Drawing code
	 }
	 */
	
}
