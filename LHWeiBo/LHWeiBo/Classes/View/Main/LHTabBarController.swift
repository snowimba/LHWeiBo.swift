//
//  LHTabBarController.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/18.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHTabBarController: UITabBarController {

	override func viewDidLoad() {
		super.viewDidLoad()

		let tabBarC : LHTabBar = LHTabBar()
//        self.view.backgroundColor
		self.setValue(tabBarC, forKeyPath: "tabBar")

		tabBarC.addBtnClickBlack = {

			let addView = LHAddView()

//            let window = UIApplication.sharedApplication().keyWindow!

			self.view.addSubview(addView)

//			window.addSubview(addView)

			addView.frame = CGRectMake(0, 0, SCREENW, SCREENH)

			addView.finishBack = { (className: String) in

				let classN = NSClassFromString(className)! as! UIViewController.Type

				let VC = classN.init()

				self.presentViewController(LHNaController(rootViewController: VC), animated: true, completion: { () -> Void in
					addView.removeFromSuperview()
				})
			}
		}

		addChildViewController()
		// Do any additional setup after loading the view.
	}

	private func addChildViewController() {

		addChildViewController(LHHomeController(), titel: "首页", imageName: "tabbar_home")
		addChildViewController(LHMesController(), titel: "消息", imageName: "tabbar_message_center")
		addChildViewController(LHDiscoverController(), titel: "发现", imageName: "tabbar_discover")
		addChildViewController(LHMeViewController(), titel: "我", imageName: "tabbar_profile")
	}

	private func addChildViewController(childController: UIViewController, titel: String, imageName: String) {

		childController.tabBarItem = LHTabBarItem()
		childController.title = titel

		addChildViewController(LHNaController(rootViewController: childController))

		childController.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.orangeColor()], forState: UIControlState.Selected)

		childController.tabBarItem.image = UIImage(named: imageName)?.imageWithRenderingMode(.AlwaysOriginal)

		childController.tabBarItem.selectedImage = UIImage(named: "\(imageName)_selected")?.imageWithRenderingMode(.AlwaysOriginal)
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
}

class LHTabBarItem: UITabBarItem {

	override var badgeValue: String? {

		didSet {

			let target = valueForKey("_target")! as! LHTabBarController

			for childV in target.tabBar.subviews {

				if childV.isKindOfClass(NSClassFromString("UITabBarButton")!) {

					for childVB in childV.subviews {

						if childVB.isKindOfClass(NSClassFromString("_UIBadgeView")!) {

							for childVBB in childVB.subviews {

								if childVBB.isKindOfClass(NSClassFromString("_UIBadgeBackground")!) {

//                                    print("nimei")
									childVBB.setValue(UIImage(named: "main_badge"), forKey: "_image")

//                                    var count : UInt32 = 0
//                                     let ivar = class_copyIvarList(NSClassFromString("_UIBadgeBackground")!, &count)
//
//                                    for i in 0..<count{
//
//                                        let str = NSString(CString: ivar_getName(ivar[Int(i)]), encoding: NSUTF8StringEncoding)
//                                        print("\(ivar[Int(i)])...\(str)")
//
//                                    }
								}
							}
						}
					}
				}
			}
		}
	}
}
