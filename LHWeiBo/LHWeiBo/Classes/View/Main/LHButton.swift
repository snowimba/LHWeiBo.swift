//
//  LHButton.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/25.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHButton : UIButton {

	override var highlighted : Bool {

		get {

			return false
		}
		set {
		}
	}

	override init(frame: CGRect) {
		super.init(frame: frame)
		setupUI()
	}

	private func setupUI() {

		titleLabel?.textAlignment = .Center
		titleLabel?.font = UIFont.systemFontOfSize(13.0)
		imageView?.contentMode = .Center
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func layoutSubviews() {
		super.layoutSubviews()
		// self.frame.origin
		imageView?.frame.size = CGSizeMake(self.frame.width, self.frame.width)

		imageView?.frame.origin = CGPointMake(0, 0)

		titleLabel?.frame.size = CGSizeMake(self.frame.width, self.frame.height - self.frame.width)
		titleLabel?.frame.origin = CGPointMake(0, self.frame.width)
	}
}