//
//  LHUnloadView.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/18.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHUnloadView: UIView {
	
	private lazy var imageBig : UIImageView = {
		
		let imageV = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
		
		imageV.sizeToFit()
		
		return imageV
		
	}()
	
	private lazy var imageRing : UIImageView = {
		
		let imageV = UIImageView(image: UIImage(named: "visitordiscover_feed_image_smallicon"))
		imageV.hidden = true
		imageV.sizeToFit()
		
		return imageV
		
	}()
	
	private lazy var imageSmall : UIImageView = {
		
		let imageV = UIImageView(image: UIImage(named: "visitordiscover_feed_mask_smallicon"))
		
		imageV.sizeToFit()
		
		
		return imageV
		
	}()
	
	private lazy var textLbl : UILabel = {
		
		let titelLbl = UILabel()
		
		titelLbl.text = "关注一些人，回这里看看有什么惊喜 关注一些人，回这里看看有什么惊喜"
		titelLbl.numberOfLines = 0
		titelLbl.font = UIFont.systemFontOfSize(13)
		titelLbl.sizeToFit()
		titelLbl.textAlignment = .Center
		titelLbl.textColor = UIColor.grayColor()
		return titelLbl
		
	}()
	
	lazy var registerBtn : UIButton = {
		
		let btn = UIButton()
		
		btn.setTitle("注册", forState: UIControlState.Normal)
		btn.titleLabel?.font = UIFont.systemFontOfSize(13.0)
		btn.setTitleColor(UIColor.orangeColor(), forState: UIControlState.Normal)
		
		btn.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
		
		return btn
		
		
	}()
	
	lazy var loginBtn : UIButton = {
		
		let btn = UIButton()
		
		btn.setTitle("登陆", forState: UIControlState.Normal)
		btn.titleLabel?.font = UIFont.systemFontOfSize(13.0)
		btn.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
		
		btn.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
		
		return btn
		
		
	}()
	
	
	override init(frame: CGRect) {
		super.init(frame: frame)
	}
	
	func setupInfo(title: String?, imageName: String?) {
		
		
		if let text = title {
			
			textLbl.text = text
			
		}
		if let name = imageName {
			
			imageBig.image = UIImage(named: name)
			
			
		} else {
			
			animation()
			
			
		}
		
		setupUI()
		
	}
	
	
	func animation() {
		
		imageRing.hidden = false
		let anim = CABasicAnimation(keyPath: "transform.rotation")
		
		anim.toValue = M_PI * 2
		
		anim.repeatCount = MAXFLOAT
		
		anim.duration = 20
		
		anim.removedOnCompletion = false
		
		imageRing.layer.addAnimation(anim, forKey: nil)
		
		
	}
	
	
	func setupUI() {
		
		addSubview(imageRing)
		addSubview(imageSmall)
		addSubview(textLbl)
		addSubview(registerBtn)
		addSubview(loginBtn)
		addSubview(imageBig)
		
		imageRing.translatesAutoresizingMaskIntoConstraints = false
		
		addConstraint(NSLayoutConstraint(item: imageRing, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
		
		addConstraint(NSLayoutConstraint(item: imageRing, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0))
		
		imageBig.translatesAutoresizingMaskIntoConstraints = false
		
		addConstraint(NSLayoutConstraint(item: imageBig, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
		
		addConstraint(NSLayoutConstraint(item: imageBig, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0))
		
		
//
		textLbl.translatesAutoresizingMaskIntoConstraints = false
//
		addConstraint(NSLayoutConstraint(item: textLbl, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: imageRing, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 16))
//
		addConstraint(NSLayoutConstraint(item: textLbl, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
		
		addConstraint(NSLayoutConstraint(item: textLbl, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 222))
		
		registerBtn.translatesAutoresizingMaskIntoConstraints = false
		//
		addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: textLbl, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 16))
		//
		addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: textLbl, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: 0))
		
		addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100))
		
		loginBtn.translatesAutoresizingMaskIntoConstraints = false
		//
		addConstraint(NSLayoutConstraint(item: loginBtn, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: registerBtn, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
		//
		addConstraint(NSLayoutConstraint(item: loginBtn, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: textLbl, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 0))
		
		addConstraint(NSLayoutConstraint(item: loginBtn, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100))
		
		
		imageSmall.translatesAutoresizingMaskIntoConstraints = false
		
		addConstraint(NSLayoutConstraint(item: imageSmall, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
		
		addConstraint(NSLayoutConstraint(item: imageSmall, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
		
		
		addConstraint(NSLayoutConstraint(item: imageSmall, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: registerBtn, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0))
		
		
//        visitordiscover_feed_mask_smallicon
//        CAAnimation
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	
	
}
