//
//  LHNaController.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/18.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHNaController: UINavigationController, UIGestureRecognizerDelegate {

	override func viewDidLoad() {
		super.viewDidLoad()

		self.interactivePopGestureRecognizer?.delegate = self
	}

	func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
		if childViewControllers.count == 1 {
            return false
		}
        return true
	}

	override func pushViewController(viewController: UIViewController, animated: Bool) {

		if childViewControllers.count > 0 {

			var title = "返回"

			if childViewControllers.count == 1 {

				title = (childViewControllers.first?.title)!
			}

			viewController.title = "第\((childViewControllers.count ?? 0))个控制器"

			viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: title, imageName: "navigationbar_back_withtext", target: self, action: "pop")

			viewController.view.backgroundColor = UIColor.blueColor()
			viewController.hidesBottomBarWhenPushed = true
		}

		super.pushViewController(viewController, animated: animated)
	}

//    override func

	func pop() {

		popViewControllerAnimated(true)
	}

//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//     self.navigationBar.barTintColor
//    }
}
