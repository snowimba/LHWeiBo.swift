
//
//  LHAddView.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/25.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit
import pop

class LHAddView: UIView {

	/*
	 // Only override drawRect: if you perform custom drawing.
	 // An empty implementation adversely affects performance during animation.
	 override func drawRect(rect: CGRect) {
	 // Drawing code
	 }
	 */

	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {

		for (index, btn) in btnArr.reverse().enumerate() {

			addAnimaForBtn(btn, index: index, isShow : false)
		}

		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(0.4 * CGFloat(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
			self.removeFromSuperview()
		}

//		removeFromSuperview()
	}

	override func willMoveToSuperview(newSuperview: UIView?) {
//		backgroundColor = UIColor.orangeColor()
		addSubview(imageV)
		addSubview(imageVT)
		imageV.snp_makeConstraints { (make) -> Void in
			make.edges.equalTo(UIEdgeInsetsZero)
		}
		imageVT.snp_makeConstraints { (make) -> Void in
			make.centerX.equalTo(self)
			make.top.equalTo(self).offset(80)
		}

		setBtnUI()
	}

	override func didMoveToSuperview() {

		for (index, btn) in btnArr.enumerate() {
//UIScrollView
			addAnimaForBtn(btn, index: index)
		}
	}

	private func setBtnUI() {

		let btnW : CGFloat = 80
		let btnH : CGFloat = 110
		let magrin : CGFloat = (SCREENW - 3 * btnW) / 4

		for i in 0..<composeArr.count {
			let com = composeArr[i]
			let btn = LHButton()
			btn.frame = CGRectMake(magrin + (btnW + magrin) * CGFloat(Int(i % 3)), (btnH + magrin) * CGFloat(Int(i / 3)) + SCREENH, btnW, btnH)
			btn.setTitle(com.title, forState: UIControlState.Normal)
			btn.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
			btn.setImage(UIImage(named: com.icon!), forState: UIControlState.Normal)
			addSubview(btn)
			btnArr.append(btn)
			btn.tag = 1 + i
			btn.addTarget(self, action: "pushSendVC:", forControlEvents: UIControlEvents.TouchUpInside)
		}
	}

	var finishBack: ((className: String) -> ())?

	@objc private func pushSendVC(btnOr: UIButton) {

		for btn in btnArr {

			if btn.tag == btnOr.tag {

				UIView.animateWithDuration(0.5, animations: { () -> Void in

					btn.transform = CGAffineTransformMakeScale(1.7, 1.7)
					btn.alpha = 0
				})
			} else {

				UIView.animateWithDuration(0.5, animations: { () -> Void in

					btn.transform = CGAffineTransformMakeScale(0.3, 0.3)
					btn.alpha = 0
				})
			}
		}

		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(0.3 * CGFloat(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in

			let className = (self.composeArr[btnOr.tag - 1].className)!

			self.finishBack?(className: className)
		}
	}

	private func addAnimaForBtn(btn: LHButton, index: Int, isShow: Bool = true) {

		let anim = POPSpringAnimation(propertyNamed: kPOPViewCenter)

		anim.toValue = NSValue(CGPoint: CGPointMake(btn.center.x, btn.center.y + (isShow ? -350 : 350)))

		anim.springSpeed = 10

		anim.springBounciness = 8

		anim.beginTime = CACurrentMediaTime() + Double(index) * 0.025

		btn.pop_addAnimation(anim, forKey: nil)
	}

	private lazy var btnArr : [LHButton] = [LHButton]()

	private lazy var composeArr : [LHComposeM] = {

		var temArrM = [LHComposeM]()

		let temArr = NSArray(contentsOfFile: NSBundle.mainBundle().pathForResource("compose.plist", ofType: nil)!)

		for value in temArr! {

			let com = LHComposeM(dict: value as! [String : AnyObject])

			temArrM.append(com)
		}

		return temArrM
	}()

	private lazy var imageV : UIImageView = {
		let imageO = UIImageView()
		imageO.image = UIImage.getBGImage().applyLightEffect()

		return imageO
	}()
	private lazy var imageVT : UIImageView = {
		let imageO = UIImageView()
		imageO.image = UIImage(named: "compose_slogan")

		return imageO
	}()
}
