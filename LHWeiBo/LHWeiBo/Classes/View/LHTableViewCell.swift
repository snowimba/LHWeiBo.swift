//
//  LHTableViewCell.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/20.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHTableViewCell: UITableViewCell {
	
    @IBOutlet weak var iconView: UIImageView!
    
    @IBOutlet weak var vipView: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var descLbl: UILabel!
    
    @IBOutlet weak var vipIcon: UIImageView!
    
    @IBAction func lookBtnAction(sender: AnyObject) {
    }

    @IBOutlet weak var titelLbl: UILabel!
    
    @IBAction func shared(sender: AnyObject) {
    }

    
    @IBAction func mesAction(sender: AnyObject) {
    }
    
    @IBAction func good(sender: UIButton) {
    }
    
    
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	override func setSelected(selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
	
}
