//
//  LHEmoticTool.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/28.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

enum LHEmotionButtonType : Int {

	case Late = 1
	case Nor = 2
	case Emoji = 3
	case Lxh = 4
}
class LHEmoticTool: UIStackView {

	override init(frame: CGRect) {
		super.init(frame: frame)
		setupUI()

//		NSNotificationCenter.defaultCenter().addObserver(self, selector: "movePage:", name: "MovePage", object: nil)
	}
//
//	@objc private func movePage(noti: NSNotification) {
//
//		let indexP = noti.object as! Int
//
//		print(indexP)
//
//		let btn = self.viewWithTag(indexP + 1) as! UIButton
//
//		btnClcik(btn)
//	}
//
//	deinit {
//
//		NSNotificationCenter.defaultCenter().removeObserver(self)
//	}

	private func setupUI() {

		addChiledBtn("compose_emotion_table_left", title: "最近", type: .Late)
		addChiledBtn("compose_emotion_table_mid", title: "默认", type: .Nor)
		addChiledBtn("compose_emotion_table_mid", title: "Emoji", type: .Emoji)
		addChiledBtn("compose_emotion_table_right", title: "浪小花", type: .Lxh)
	}

	func shouldClickBtn(index: Int) {

		let btn = viewWithTag(index + 1) as! UIButton

		if lastBtn == btn {

			return
		}
		btn.selected = true

		lastBtn?.selected = false

		lastBtn = btn
	}

	private func addChiledBtn(imageName: String, title: String, type: LHEmotionButtonType) {

		let btn = UIButton()
		btn.tag = type.rawValue
		btn.setBackgroundImage(UIImage(named: "\(imageName)_normal"), forState: UIControlState.Normal)
		btn.setBackgroundImage(UIImage(named: "\(imageName)_selected"), forState: UIControlState.Selected)
		btn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
		btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Selected)
		btn.setTitle(title, forState: UIControlState.Normal)
		btn.titleLabel?.font = UIFont.systemFontOfSize(14.0)
		btn.addTarget(self, action: "btnClcik:", forControlEvents: UIControlEvents.TouchUpInside)
		addArrangedSubview(btn)
		if type == .Late {

			btnClcik(btn)
		}
	}

	private var lastBtn : UIButton?

	@objc private func btnClcik(btn: UIButton) {

		if lastBtn == btn {

			return
		}

		btn.selected = true

		lastBtn?.selected = false

		lastBtn = btn

		NSNotificationCenter.defaultCenter().postNotificationName("changeCell", object: btn.tag - 1)
//		changeCellBlack?(type: LHEmotionButtonType(rawValue: btn.tag)!)
	}
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
