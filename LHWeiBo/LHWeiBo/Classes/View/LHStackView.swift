//
//  LHStackView.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/27.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

protocol LHStackViewDelegetWithBtn : NSObjectProtocol {

	func stackViewBtnClick(type: LHToolBarButtonType)
}

enum LHToolBarButtonType : Int {

	case Pic = 0
	case Trend = 1
	case Mention = 2
	case Emoticon = 3
	case Add = 4
}
class LHStackView: UIStackView {

	override init(frame: CGRect) {
		super.init(frame: frame)
		setupUI()
	}

	weak var delegeBtn : LHStackViewDelegetWithBtn?

	private func setupUI() {

		addChiledBtn("compose_toolbar_picture", type: .Pic)
		addChiledBtn("compose_trendbutton_background", type: .Trend)
		addChiledBtn("compose_mentionbutton_background", type: .Mention)
		addChiledBtn("compose_emoticonbutton_background", type: .Emoticon)
		addChiledBtn("compose_add_background", type: .Add)
	}

	private func addChiledBtn(imageName: String, type: LHToolBarButtonType) {

		let btn = UIButton()

		btn.tag = type.rawValue

		btn.setBackgroundImage(UIImage(named: "compose_toolbar_background"), forState: UIControlState.Normal)
		btn.setBackgroundImage(UIImage(named: "compose_toolbar_background"), forState: UIControlState.Highlighted)

		btn.setImage(UIImage(named: imageName), forState: UIControlState.Normal)
		btn.setImage(UIImage(named: "\(imageName)_highlighted"), forState: UIControlState.Highlighted)
		btn.addTarget(self, action: "btnClcik:", forControlEvents: UIControlEvents.TouchUpInside)
		addArrangedSubview(btn)
	}

	private var isClcik : Bool = true

	@objc private func btnClcik(btn: UIButton) {

		if LHToolBarButtonType(rawValue: btn.tag)! == .Emoticon {

			var imageName = String()

			if isClcik {

				imageName = "compose_keyboardbutton_background"
			} else {

				imageName = "compose_emoticonbutton_background"
			}
			btn.setImage(UIImage(named: imageName), forState: UIControlState.Normal)
			btn.setImage(UIImage(named: "\(imageName)_highlighted"), forState: UIControlState.Highlighted)
			isClcik = !isClcik
		}

		delegeBtn?.stackViewBtnClick(LHToolBarButtonType(rawValue: btn.tag)!)
	}
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
