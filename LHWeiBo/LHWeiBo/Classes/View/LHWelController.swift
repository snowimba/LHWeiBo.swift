//
//  LHWelController.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/21.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit
import SDWebImage
import SnapKit

class LHWelController: UIViewController {

	private lazy var iconImage : UIImageView = {
		let imageView = UIImageView()

		imageView.sd_setImageWithURL(NSURL(string: LHLoginAboutViewModel.sharedViewModel.userInfo?.avatar_large ?? ""), placeholderImage: UIImage(named: "avatar_default_big"))

//        imageView.sd_setImageWithURL(NSURL(string: LHLoginAboutViewModel.sharedViewModel.userInfo?.avatar_large ?? ""), placeholderImage: UIImage(named: "avatar_default_big"), completed: { (image, error, _, _) -> Void in
//            print(image)
//        })

		imageView.layer.cornerRadius = 45
		imageView.layer.masksToBounds = true
		return imageView
	}()

	private lazy var titelLbl : UILabel = {

		let lbl = UILabel()

		lbl.text = "欢迎回来"
		lbl.font = UIFont.systemFontOfSize(14.0)
		lbl.textColor = UIColor.grayColor()

		lbl.alpha = 0
		return lbl
	}()

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
		view.backgroundColor = UIColor(white: 247 / 255, alpha: 1)

		setupUI()
	}
	func setupUI() {

		view.backgroundColor = UIColor(patternImage: UIImage(named: "ad_backgrounde")!)

		view.addSubview(iconImage)
		view.addSubview(titelLbl)

		iconImage.snp_makeConstraints { (make) -> Void in

			make.center.equalTo(view)
		}

		titelLbl.snp_makeConstraints { (make) -> Void in
			make.centerX.equalTo(view)
			make.top.equalTo(iconImage.snp_bottom).offset(16)
		}
	}

	override func viewDidAppear(animated: Bool) {

		iconImage.snp_updateConstraints { (make) -> Void in
			make.centerY.equalTo(view).offset(-150)
		}

		UIView.animateWithDuration(2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: { () -> Void in

			self.view.layoutIfNeeded()
		}) { (_) -> Void in

			UIView.animateWithDuration(1, animations: { () -> Void in
				self.titelLbl.alpha = 1
			}, completion: { (_) -> Void in

				NSNotificationCenter.defaultCenter().postNotificationName("changeRootVC", object: nil)
			})
		}
	}
}
