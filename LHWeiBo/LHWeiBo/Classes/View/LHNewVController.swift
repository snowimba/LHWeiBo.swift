//
//  LHNewVController.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/21.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit
import SnapKit
class LHNewVController: UIViewController {

	let count = 4

	private lazy var scroll : UIScrollView = {

		let scrollV = UIScrollView()

		scrollV.bounces = false

		scrollV.showsHorizontalScrollIndicator = false

		scrollV.pagingEnabled = true

		scrollV.delegate = self

		return scrollV
	}()

	private lazy var pageView : UIPageControl = {

		let pageV = UIPageControl()

		pageV.currentPageIndicatorTintColor = UIColor.orangeColor()

		pageV.pageIndicatorTintColor = UIColor.purpleColor()

		pageV.numberOfPages = self.count

		pageV.userInteractionEnabled = false
		return pageV
	}()

	private lazy var sharedBtn : UIButton = {

		let btnN = UIButton()

		btnN.setTitle("分享到微博", forState: UIControlState.Normal)

		btnN.setImage(UIImage(named: "new_feature_share_false"), forState: UIControlState.Normal)

		btnN.setImage(UIImage(named: "new_feature_share_true"), forState: UIControlState.Selected)
		btnN.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
		btnN.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5)
		btnN.titleLabel?.font = UIFont.systemFontOfSize(14.0)

		btnN.addTarget(self, action: "sharedBtnClcik:", forControlEvents: UIControlEvents.TouchUpInside)
		return btnN
	}()

	private lazy var pushBtn : UIButton = {

		let btnN = UIButton()

		btnN.setTitle("进入微博", forState: UIControlState.Normal)

		btnN.setBackgroundImage(UIImage(named: "new_feature_finish_button"), forState: UIControlState.Normal)

		btnN.setBackgroundImage(UIImage(named: "new_feature_finish_button_highlighted"), forState: UIControlState.Highlighted)
		btnN.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)

		btnN.titleLabel?.font = UIFont.systemFontOfSize(14.0)
//        btnN.titleEdgeInsets = UIEdgeInsetsMake(5, 0, 0, -5)
		btnN.addTarget(self, action: "pushNextVC", forControlEvents: UIControlEvents.TouchUpInside)
		return btnN
	}()

	@objc private func pushNextVC() {
        NSNotificationCenter.defaultCenter().postNotificationName("changeRootVC", object: nil)
	}

	@objc private func sharedBtnClcik(btn: UIButton) {

		btn.selected = !btn.selected
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.

		setupUI()
	}

	func setupUI() {

		view.addSubview(scroll)
		view.addSubview(pageView)

//        scroll.frame = CGRectMake(0, 0, SCREENW, SCREENH)

		scroll.snp_makeConstraints { (make) -> Void in
			make.edges.equalTo(UIEdgeInsetsZero)
		}

		pageView.snp_makeConstraints { (make) -> Void in
			make.centerX.equalTo(view)
			make.bottom.equalTo(view).offset(-80)
		}

		for i in 0..<count {

			let imageV = UIImageView()

			imageV.image = UIImage(named: "new_feature_\(i+1)")

			imageV.frame = CGRectMake(CGFloat(i) * SCREENW, 0, SCREENW, SCREENH)

			scroll.addSubview(imageV)

			if i == count - 1 {

				setLastImageView(imageV)
			}
		}

		scroll.contentSize = CGSizeMake(CGFloat(count) * SCREENW, 0)
	}

	func setLastImageView(imageView: UIImageView) {

		imageView.userInteractionEnabled = true
		imageView.addSubview(sharedBtn)
		imageView.addSubview(pushBtn)

		sharedBtn.snp_makeConstraints { (make) -> Void in
			make.centerX.equalTo(imageView)
			make.centerY.equalTo(imageView).offset(80)
		}

		pushBtn.snp_makeConstraints { (make) -> Void in
			make.centerX.equalTo(imageView)
			make.top.equalTo(sharedBtn.snp_bottom).offset(18)
		}
	}
}

extension LHNewVController : UIScrollViewDelegate {

	func scrollViewDidScroll(scrollView: UIScrollView) {

		pageView.currentPage = Int(scrollView.contentOffset.x / SCREENW + 0.5)
	}
}
