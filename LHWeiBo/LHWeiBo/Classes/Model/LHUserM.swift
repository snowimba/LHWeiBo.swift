//
//  LHUserM.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/22.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHUserM: NSObject {

    var screen_name : String?
 
//    var remark : String?
    
    var profile_image_url :String?
    
    var mbrank : Int = 0
    
    var verified : Int = -1
    
    init(dict: [String: AnyObject]) {
        
        super.init()
        
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
    }
}
