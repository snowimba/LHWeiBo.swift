//
//  LHEonticM.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/28.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHEonticM: YHCoderObject {
	var chs : String?
	var png : String?
	var type : String?
	var code : String?
	var name : String?

	init(dict: [String: AnyObject]) {

		super.init()

		setValuesForKeysWithDictionary(dict)
	}

	override func setValue(value: AnyObject?, forUndefinedKey key: String) {
	}
}
