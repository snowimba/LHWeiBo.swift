
//
//  LHStatusM.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/22.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHStatusM: NSObject {

    var created_at : String?{
        
        didSet {
            
            createdDate = NSDate.getDateWithString(created_at!)
            
        }
    }
	var source : String?
	var user : LHUserM?
	var text : String?
	var reposts_count : Int = 0
	var comments_count : Int = 0
	var attitudes_count : Int = 0
	var retweeted_status : LHStatusM?
    var createdDate : NSDate?
	var pic_urls : [LHPicM]?
	var id : Int = 0

	init(dict: [String: AnyObject]) {

		super.init()

		setValuesForKeysWithDictionary(dict)
	}

	override func setValue(value: AnyObject?, forKey key: String) {

		if key == "user" {

			self.user = LHUserM(dict: value as! [String: AnyObject])

			return
		} else if key == "retweeted_status" {

			self.retweeted_status = LHStatusM(dict: value as! [String: AnyObject])
			return
		} else if key == "pic_urls" {

			var tempArray = [LHPicM]()
			for dict in(value as! [[String: AnyObject]]) {
				let photoInfo = LHPicM(dict: dict)
				tempArray.append(photoInfo)
			}
			pic_urls = tempArray
			return
		}

		super.setValue(value, forKey: key)
	}

	override func setValue(value: AnyObject?, forUndefinedKey key: String) {
	}
}
