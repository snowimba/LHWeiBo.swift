//
//  LHUserInfo.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/21.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHUserInfo: YHCoderObject {
	
	var uid: String?
	
	var expires_in: NSTimeInterval = 0 {
		
		didSet {
			
			overdueDate = NSDate().dateByAddingTimeInterval(expires_in)
		}
	}
	
	var access_token: String?
	
	var remind_in: String?
	
	var screen_name: String?
	
	var avatar_large: String?
	
	var overdueDate : NSDate?
	
	init(dict: [String: AnyObject]) {
		
		super.init()
		
		setValuesForKeysWithDictionary(dict)
	}
	
	override func setValue(value: AnyObject?, forUndefinedKey key: String) {
	}
}
