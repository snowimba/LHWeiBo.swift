//
//  LHPicM.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/24.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHPicM: NSObject {

	var thumbnail_pic : String?
	var rect : CGRect?
	init(dict: [String: AnyObject]) {

		super.init()

		setValuesForKeysWithDictionary(dict)
	}

	override func setValue(value: AnyObject?, forUndefinedKey key: String) {
	}
}
