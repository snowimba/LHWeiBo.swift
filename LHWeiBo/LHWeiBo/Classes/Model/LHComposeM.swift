//
//  LHComposeM.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/25.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

class LHComposeM: NSObject {

	var title : String?
	var icon : String?
	var className : String?

	init(dict: [String: AnyObject]) {

		super.init()

		setValuesForKeysWithDictionary(dict)
	}

	override func setValue(value: AnyObject?, forUndefinedKey key: String) {
	}
}
