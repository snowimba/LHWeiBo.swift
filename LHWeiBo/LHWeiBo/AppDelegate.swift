//
//  AppDelegate.swift
//  LHWeiBo
//
//  Created by snowimba on 16/1/18.
//  Copyright © 2016年 snowimba. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		// Override point for customization after application launch

		window = UIWindow(frame: UIScreen.mainScreen().bounds)

		window?.rootViewController = LHLoginAboutViewModel.sharedViewModel.isLogin ? defaulVC(): LHTabBarController()

		window?.makeKeyAndVisible()

		NSNotificationCenter.defaultCenter().addObserver(self, selector: "changeRootVC:", name: "changeRootVC", object: nil)

		return true
	}
	deinit {

		NSNotificationCenter.defaultCenter().removeObserver(self)
	}

	func changeRootVC(noti: NSNotification) {

		if noti.object != nil {
			window?.rootViewController = LHWelController()

			return
		}

		window?.rootViewController = LHTabBarController()
	}

	func defaulVC() -> UIViewController {

		let version = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as! String

		let shabox = NSUserDefaults.standardUserDefaults().stringForKey("version")

		if version == shabox {

			return LHWelController()
		}

		NSUserDefaults.standardUserDefaults().setObject(version, forKey: "version")

		return LHNewVController()
	}
}

